# Kubernetes Namespaces

Define the kubernetes namespaces, teams and right associated to them.

## Adding a namespace.
Add a file with the following content : 
```
---
appName: NEW_NAMESPACE_NAME
dns: 
  YOUR_DNS: admin-gateway   # for dns that sould be on the 9999 vpn restricted port (admin ui)
  YOUR_DNS_2: standard-gateway # for dns that should be available on the https 443 port
permissions:    # for human permissions (roles are defined in https://gitlab.com/agepoly/it/kubernetes-cluster-configuration/-/tree/main/roles) 
  GROUP_NAME: ROLE_NAME #available roles are read, write, write-privileged
gitlabProjects:
  GITLAB_PROJECT_ID: GITLAB_PROJECT_PATH  # give access rights to repo listed here on the namespace
privilegedCI: false  # set true give the ci in the projects extended cluster scoped rights (crd, ...)
maxCPU: "2"
maxMemory: "2Gi"
```

### Having one or more app repo
Add an entry in the right section :)
